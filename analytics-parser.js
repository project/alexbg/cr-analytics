const chromeLauncher = require('chrome-launcher');
const CDP = require('chrome-remote-interface');
const delay = require('delay');
const fs = require('fs');
const settings = require('./settings').load();

const PUBLISHER = "ux-publisher-name";
const CUSTOMER = "ux-user-name";
const scripts = {
    getInstalls: "document.querySelector('.ux-item-rating span').textContent",
    getLastVersion: "Array.from(document.querySelector('.ux-table-metadata').getElementsByTagName('div')).map(x => x.innerText)",
    clickRating: "(document.getElementById('Pivot0-Tab2') || document.getElementById('Pivot0-Tab1')).click()",
    getUserName: `Array.from(document.querySelectorAll('.${CUSTOMER}, .${PUBLISHER}')).map(x => { return { className: x.className, name: x.innerText }})`,
    getReviewRating: "Array.from(document.querySelectorAll('.ms-Rating-star')).map(x => x.getAttribute('aria-label'))",
    getReviewUpdated: "Array.from(document.querySelectorAll('.ux-updated-date')).map(x => x.innerText)",
    getReviewText: "Array.from(document.querySelectorAll('.react-read-more-text')).map(x => x.innerText)",
};

function retreiveProductInfo(protocol, product) {
    const {Page, Runtime} = protocol;
    return new Promise(async(resolve, reject) => {
        protocol.once('Page.loadEventFired', async() => {
            var productInfo = {};
            await retrieveInstalls(Runtime, productInfo)
            .retrieveLastVersion(Runtime, productInfo)
            .catch(e => reject(e))
            .then(() => resolve(productInfo))
        });

        Page.navigate({ url: product.url });
    })
    .then(
        productInfo =>
            Promise.resolve({ 
                name: product.name, 
                installs: productInfo.installs,
                lastVersion: productInfo.lastVersion
            }),                      
        e => 
            Promise.resolve({
                name: product.name,
                error: e.toString()
            })
    )
}
function retrieveNewReviews(protocol, product, productInfo) {
    const {Page, Runtime} = protocol;
    return Runtime.evaluate({ returnByValue: true, expression: scripts.clickRating })
    .then(() => 
        delay(settings.delayAfterClick)
    )
    .then(
        () => 
            retrieveReviews(Runtime),
        e => rethrowError(e)
    )
    .then(
        reviews => {
            productInfo.reviews = reviews;
            return Promise.resolve(productInfo);
        },
        e => {
            productInfo.error = e.toString();
            return Promise.resolve(productInfo);
        }
    );
}
function rethrowError(e) {
    throw e;
}
function validate(result) {  
    if(result.value !== undefined) 
        return result.value;
    
    throw new Error(result.className == "TypeError" ? result.description: result.toString());
}
function captureScreenshot(Page) {
    return Page.captureScreenshot({format: 'png', fromSurface: true})
    .then(ss => {
        fs.writeFileSync('screenshot.png', ss.data, 'base64', function(err) {
            if (err) console.log(err);
        });
        return Promise.resolve();
    });
}
function retrieveReviews(Runtime) {
    var reviews = {
        count: function() { return this.users === undefined ? 0: this.users.length; },
        customersCount: function() { return this.users.filter(x => x.className == CUSTOMER).length; },
        publishersCount: function() { return this.users.filter(x => x.className == PUBLISHER).length; },
        validateAndThrow: function() {
            var count = this.count();
            var customersCount = this.customersCount();
            var publishersCount = this.publishersCount();
            if(count != this.updated.length)
                throw new Error("count != this.updated.length");
            if(count != this.texts.length)
                throw new Error("count != this.updated.length");
            if(count != customersCount + publishersCount)
                throw new Error("count != customersCount + publishersCount");
            if(customersCount != this.ratings.length)
                throw new Error("customersCount != this.ratings.length");
        }
    };
    return Runtime.evaluate({ returnByValue: true, expression: scripts.getUserName })
    .then(
        ({result}) => 
            reviews.users = validate(result),
        e => rethrowError(e)
    )
    .then(
        () => Runtime.evaluate({ returnByValue: true, expression: scripts.getReviewRating }),
        e => rethrowError(e)
    )
    .then(
        ({result}) => 
            reviews.ratings = validate(result),
        e => rethrowError(e)
    )
    .then(
        () => Runtime.evaluate({ returnByValue: true, expression: scripts.getReviewUpdated }),
        e => rethrowError(e)
    )
    .then(
        ({result}) => 
            reviews.updated = validate(result),
        e => rethrowError(e)
    )
    .then(
        () => Runtime.evaluate({ returnByValue: true, expression: scripts.getReviewText }),
        e => rethrowError(e)
    )
    .then(
        ({result}) => 
            reviews.texts = validate(result),
        e => rethrowError(e)
    )
    .then(
        () => 
            Promise.resolve(beautifyReviews(reviews)),
        e => {
            reviews.error = e.toString();
            return Promise.resolve(reviews);
        }
    );
}
function beautifyReviews(reviews) {
    reviews.validateAndThrow();
    
    var ratingIndex = 0;
    var newReviews = [];
    for(var i=0; i<reviews.users.length; i++) {
        var user = reviews.users[i];
        var result = {
            userName: user.name,
            updated: reviews.updated[i],
            rating: user.className == CUSTOMER ? reviews.ratings[ratingIndex ++] : undefined,
            text: reviews.texts[i]
        };        
        newReviews.push(result);
    }
    return newReviews;
}
function retrieveInstalls(Runtime, productInfo) {
    return Runtime.evaluate({ expression: scripts.getInstalls })
    .then(
        ({ result }) => productInfo.installs = validate(result),                           
        e => rethrowError(e)
    );
}
Promise.prototype.retrieveLastVersion = function retrieveLastVersion(Runtime, productInfo) {
    return this.then(
        () => Runtime.evaluate({ returnByValue: true, expression: scripts.getLastVersion }),
        e => rethrowError(e)
    )
    .then(
        ({ result }) => {
            var value = validate(result);
            if(value.length < 4)
                throw new Error(`Parsing Error: Invalid line count: ${value.length}`);

            productInfo.lastVersion = `${value[0]} ${value[1]}\n${value[2]} ${value[3]}`;
        },
        e => rethrowError(e)
    );
}
function getReviewIndex(reviewList, review) {
    for(var i=0; i<reviewList.length; i++) {
        var item = reviewList[i];
        if(item.userName == review.userName 
        && item.rating == review.rating 
        && item.text == review.text)
            return i;
    }
    return -1;
}
function filterOutReviews(prevReviews, reviews) {
    var result = [];
    for(var i=0; i<reviews.length; i++) {
        var review = reviews[i];        
        if(getReviewIndex(prevReviews, reviews[i]) < 0)
            result.push(review);
    }
    return result;
}
exports.getResultJsonPath = function getResultJsonPath(index) {
    if(!fs.existsSync(settings.storagePath))
        return;

    var files = fs.readdirSync(settings.storagePath).sort();
    var fileName = files[files.length - 1 - index];
    if(fileName)
        return settings.storagePath + fileName;
}
exports.loadResult = function loadResult(index) {
    var filePath = this.getResultJsonPath(index);
    if(!filePath)
        return;
        
    var data = fs.readFileSync(filePath, 'utf8');
    var lastInfos = JSON.parse(data);
    return lastInfos;
}
exports.filterOutLastReviews = function filterOutLastReviews(prevInfos, infos) {
    if(!prevInfos)
        return infos;
        
    for(var i=0; i<infos.length; i++) {
        var info = infos[i];
        var prevInfo = prevInfos[i];
        if(prevInfo.name != info.name)
            continue;

        info.reviews = filterOutReviews(prevInfo.reviews, info.reviews);
    }
    return infos;
}
exports.store = function store(info) {
    if(!fs.existsSync(settings.storagePath))
        fs.mkdirSync(settings.storagePath);

    var jsonString = JSON.stringify(info, null, 4);
    fs.writeFileSync(settings.storagePath + `info-${Date.now()}.json`, jsonString);
}
exports.storeDiff = function storeDiff(info) {
    if(!fs.existsSync(settings.storagePath))
        fs.mkdirSync(settings.storagePath);

    var jsonString = JSON.stringify(info, null, 4);
    fs.writeFileSync(settings.storagePath + `info-diff.json`, jsonString);
}
exports.request = function request(products) {
    var promise = new Promise((resolve, reject) => {
        var ctx = {};
        chromeLauncher.launch({ chromeFlags: [ '--disable-gpu', '--headless' ] })
        .then(chrome => {
            ctx.chrome = chrome;
            return CDP({ port: chrome.port });
        })
        .then(async(protocol) => { 
            const { DOM, Page, Runtime } = ctx.protocol = protocol;
            await Promise.all([ Page.enable(), Runtime.enable(), DOM.enable() ]);        
            return protocol;
        })
        .then(async(protocol) => {
            var results = [];
            for(var i=0; i<products.length; i++) {
                var product = products[i];
                await retreiveProductInfo(protocol, product)
                .then(info => retrieveNewReviews(protocol, product, info))
                .then(info => {
                    console.log(info);
                    results.push(info)
                });
            }

            resolve(results);
        })
        .finally(() => {
            ctx.protocol.close();
            ctx.chrome.kill();
        });
    });
    return promise;
};