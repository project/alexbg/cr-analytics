const assert = require('assert');
const parser = require("./analytics-parser");
const settings = require("./settings").load();

describe("analytics-parser", function() {
    it("retreives installation count", async function() {
        var result = await parser.request(settings.products);
        assert.equal(settings.products.length, result.length);
        parser.store(result);
        
        var lastResult = parser.loadResult(0);
        var prevResult = parser.loadResult(1);
        var diff = parser.filterOutLastReviews(prevResult, lastResult);
        assert.equal(settings.products.length, diff.length);
    });
});