const fs = require('fs');
const {google} = require('googleapis');
const https = require('https');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = 'credentials.json';

fs.readFile('client_secret.json', (err, content) => {
    if (err)
        return console.log('Error loading client secret file:', err);
    authorize(JSON.parse(content), listMajors);
});

function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
  
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) 
            return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);

    //   const rl = readline.createInterface({
    //     input: process.stdin,
    //     output: process.stdout,
    //   });
    //   rl.question('Enter the code from that page here: ', (code) => {
    //     rl.close();
    oAuth2Client.getToken("INSERT YOUR TOKEN", (err, token) => {
        if (err) 
            return callback(err);
        oAuth2Client.setCredentials(token);        
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
            if (err) 
                console.error(err);
            console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
    });
    //});
}

function listMajors(auth) {
    const sheets = google.sheets({version: 'v4', auth});
    sheets.spreadsheets.values.get({
        spreadsheetId: 'xxx', // CRR Analytics Spreadsheet ID
        range: 'Downloads!A1:B',
    }, 
    (err, {data}) => {
        if (err) 
            return console.log('The API returned an error: ' + err);
        const rows = data.values;
        if (rows.length) {
            console.log('Name, Major:');
            // Print columns A and E, which correspond to indices 0 and 4.
            rows.map((row) => {
                console.log(`${row[0]}, ${row[1]}`);
            });
        } else {
            console.log('No data found.');
        }
    });
}
/// Google SpreadSheet API v4
// var requests = [];
// requests.push({
//   "repeatCell": { /// <<<< repeat cell
//     "range": {
//       "sheetId": yourSheetId,
//       "startRowIndex": 1,
//       "endRowIndex": 2,
//       "startColumnIndex": 0,
//       "endColumnIndex": 1
//     },
//     "cell": {
//       note: "Your note"   /// <<<< setting note to cell
//     },
//     "fields": "note"
//   }
// });

// gapi.client.sheets.spreadsheets.batchUpdate({
//   spreadsheetId: yourDocumentId,
//   requests: requests
// }).then(function(response) {
//   console.log(response);
//   callback();
// });