const assert = require('assert');

describe("load", function() {
    it("loads settings from JSON", function() {
        const settings = require("./settings").load();
        assert.equal(9, settings.products.length);               
    });
});