const fs = require("fs");
const fileName = "settings.json";

var settings = null;
exports.load = function load() {
    if(settings != null)
        return settings;

    var settingsData = fs.readFileSync(fileName, 'utf8');
    settings = JSON.parse(settingsData);
    settings.getUrls = function getUrls() {
        var result = [];
        for(var i=0; i<this.products.length; i++)
            result.push(this.products[i].url);
        return result;
    };
    return settings;
}