const nodemailer = require('nodemailer');
const fs = require('fs');
const settings = require("./settings").load();
const parser = require("./analytics-parser");

function build(tag, attributes, content) {
    var text = typeof(content) == 'function' ? content() : content;
    if(text)
        return  attributes 
            ? `<${tag} ${attributes}>${text}</${tag}>`
            : `<${tag}>${text}</${tag}>`;
    
    return  attributes ? `<${tag} ${attributes} />` : `<${tag} />`;
}
function sendEmail(subject, path) {
    var transporter = nodemailer.createTransport({
        service: settings.emailReport.service,
        auth: {
            user: settings.emailReport.user,
            pass: settings.emailReport.pass
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    var htmlstream = fs.createReadStream(path);
    var mailOptions = {
        from: settings.emailReport.user,
        to: settings.emailReport.to,
        subject: subject,
        html: htmlstream,
        attachments: [
            {
                filename: 'lastResults.json',
                path: parser.getResultJsonPath(0)
            }
        ]
    };

    transporter.sendMail(mailOptions, function(error, info) {
        console.log(error ? error : 'Email sent: ' + info.response);
    });
}
function buildProduct(item) {
    return build('table', 'style="width: 600px; height: auto; margin: 0 auto; border-spacing: 0px; border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 14px;" class="productItem"', () =>
        build('tr', null, () =>
            build('td',  'class="productItem" colspan="3" style="background-color: #eff1f3; width: inherit; margin: 0; font-size: 30px; font-weight: 600; padding-bottom: 20px; padding-left: 10px; padding-top: 20px;"', item.name)
        )
        + build('tr', null, () =>
            build('td', 'class="installs" style="padding-right: 30px; padding-left: 10px; vertical-align: 0; background-color: #eff1f3; color: #666;"', () => 
                item.error
                ? item.error
                : item.installs
                + build('span', 'class="installsUpdater" style="padding-left: 10px; font-weight: 600; color: green;"', '+1288')
            )
            + (
                item.error
                ? ''
                : build('td', 'class="version" style="padding-right: 30px; padding-bottom: 20px; vertical-align: 0; background-color: #eff1f3; color: #666;"', item.lastVersion.split('\n')[0])
                + build('td', 'class="lastUpdated" style="padding-right: 30px; vertical-align: 0; background-color: #eff1f3; color: #666;"', item.lastVersion.split('\n')[1])
            )
        )
        + buildReviewList(item.reviews)
    );
}
function buildReviewList(reviews) {
    if(!reviews || reviews.length == 0)
        return '';
    
    return build('tr', null, () => 
        build('td', 'colspan="3" style="font-size: 24px; font-weight: 400; color: #333; padding-left: 10px; padding-top: 30px; margin: 0; padding-bottom: 20px;"',
            'User Reviews') 
    ) 
    + reviews.reduce((prev, current) => 
        typeof(prev) == 'string'
        ? prev + buildReview(current)
        : buildReview(prev) + buildReview(current)
    );
}
function buildReviewWithRating(review) {
    return build('tr', null, () =>
        build('td', 'class="userName" style="color: #333; border-top: 1px solid #eff1f3; font-size: 16px; font-weight: bold; padding-left: 10px; padding-top: 20px;";',
            review.userName)
        + build('td', 'class="rating" colspan="2" style="color: red; font-weight: bold; padding-top: 20px;"', 
            review.rating)
    )
    + build('tr', null, () =>
        build('td', 'class="updated" colspan="3" style="padding-left: 10px; padding-top: 10px; color: #666;"',
            review.updated)
    )
    + build('tr', null, () =>
        build('td', 'class="textReviews" colspan="3" style="color: #333; padding-left: 10px; padding-top: 10px; padding-bottom: 20px;"',
            review.text)
    );
}
function buildAnswer(answer) {
    return build('tr', null, () =>
        build('td', 'class=class="userName" colspan="3" style="color: #333; border-top: 1px solid #eff1f3; font-size: 16px; font-weight: bold; padding-left: 100px; padding-top: 20px;"',
            answer.userName)
    )
    + build('tr', null, () =>
        build('td', 'class="updated" colspan="3" style="padding-left: 10px; padding-top: 10px; padding-left: 100px; font-size: 14px; color: #666;"',
            answer.updated)
    )
    + build('tr', null, () =>
        build('td', 'class="textReviews" colspan="3" style="color: #333; padding-left: 10px; padding-top: 10px; padding-left: 100px; padding-bottom: 20px;"',
            answer.text)
    );
}
function buildReview(review) {
    return review.rating ? buildReviewWithRating(review) : buildAnswer(review);
}
function buildReport() {
    return  build('html', 'lang="en"', () => 
        build('head', null,  () => 
            build('meta', 'charset="UTF-8"')
            + build('title', null, 'VisualStudio MarketPlace Statistics')
        )
        + build('body', '', () => {
            var lastResult = parser.loadResult(0);
            var prevResult = parser.loadResult(1);
            var diff = parser.filterOutLastReviews(prevResult, lastResult);
            return diff.reduce((prev, item) => 
                typeof(prev) == 'string' 
                ? prev + buildProduct(item)
                : buildProduct(prev) + buildProduct(item)
            );
        })
    );
}

var text = buildReport();
var reportPath = './report.html';
fs.writeFileSync(reportPath, text);
sendEmail('Weekly VS MarketPlace Statistics', reportPath);